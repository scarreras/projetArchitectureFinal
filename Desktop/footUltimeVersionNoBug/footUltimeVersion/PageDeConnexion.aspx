﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/Site.Master" CodeBehind="PageDeConnexion.aspx.cs" Inherits="projetfoot.PageDeConnexion" %>

    <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
         <link rel="stylesheet" href="Content/PageInscription.css"/>
       

    <div class="body body-s">
         <div class="sky-form">
				<header class="header1">Connexion</header>
         				
				<fieldset>					
					<section>
						<div class="row">
							<label class="label col col-4" style="color:black; font-size:14px">Pseudo</label>
							<div class="col col-8">
								<label class="input"">
									<i class="icon-append icon-user"></i>
									<input id="login" type="text" runat="server">
								</label>
							</div>
						</div>
					</section>
					
					<section>
						<div class="row">
							<label class="label col col-4" style="color:black; font-size:14px">Mot de passe</label>
							<div class="col col-8">
								<label class="input">
									<i class="icon-append icon-lock"></i>
									<input id="mdp" type="password" runat="server">
								</label>
								<div class="note1"><a href="#">Mot de passe oublié ?</a></div>
							</div>
						</div>
					</section>
					
					<section>
						<div class="row">
							<div class="col col-4"></div>
							<div class="col col-8">
								<label class="checkbox"><input type="checkbox" name="checkbox-inline" checked><i></i>Rester connecté</label>
							</div>
						</div>
					</section>
				</fieldset>
				<footer>
					<button id="bouton_connexion" type="submit" class="button" runat="server" onserverclick="connexion_button_Click">Connexion</button>
					<a href="PageInscription.aspx" class="button button-secondary">Inscription</a>
				</footer>
			
        </div>
            </div>
      
</asp:Content>


