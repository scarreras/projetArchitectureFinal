﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace projetfoot
{
    public partial class PageDeConnexion : System.Web.UI.Page
    {
        Manager.UtilisateurManager utilisateur = new Manager.UtilisateurManager();
        //private string login;
        //private string mdp;

        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void textbox_login_TextChanged(object sender, EventArgs e)
        {

        }

        protected void textbox_mdp_TextChanged(object sender, EventArgs e)
        {

        }

        protected void connexion_button_Click(object sender, EventArgs e)
        {
            bool connexion = utilisateur.connexionUtilisateur(login.Value, mdp.Value);

            if(connexion == true)
            {
                string id_util = utilisateur.recupererIdUtil(login.Value);
                SessionManager.Id_utilisateur = id_util;
                SessionManager.Login = login.Value;
                Page.Response.Redirect("Default.aspx");
            }
            
            
        }
    }
}