﻿using footUltimeVersion.Manager;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace footUltimeVersion.Confrontation
{
    public partial class AjouterConfrontation : System.Web.UI.Page
    {
        ConfrontationManager manager_confrontation = new ConfrontationManager();
        string jour;
        string date;
        string equipe1;
        string equipe2;

        protected void Page_Load(object sender, EventArgs e)
        {
            afficherJournees();
            afficherEquipes();
        }

        protected void button_valider(object sender, EventArgs e)
        {
            jour = dropdownlist_journee.Text;
            date = textbox_date.Text;
            equipe1 = dropdownlist_equipe1.Text;
            equipe2 = dropdownlist_equipe2.Text;

            long lastId = manager_confrontation.ajouterConfrontation(date, jour);

            int idConf = (int)lastId;

            manager_confrontation.ajouterEquipe1(equipe1, idConf);
            manager_confrontation.ajouterEquipe2(equipe2, idConf);

        }

        private void afficherJournees()
        {
            List<string[]> liste_journees = manager_confrontation.recupererJournees();

            dropdownlist_journee.Items.Add(new ListItem(""));

            for (int i = 0; i < liste_journees.Count; i++)
            {
                dropdownlist_journee.Items.Add(new ListItem(liste_journees.ElementAt(i)[0].ToString()));
            }

        }

        private void afficherEquipes()
        {
            List<string[]> liste_equipes = manager_confrontation.recupererEquipes();

            dropdownlist_equipe1.Items.Add(new ListItem(""));
            dropdownlist_equipe2.Items.Add(new ListItem(""));

            for (int i = 0; i < liste_equipes.Count; i++)
            {
                dropdownlist_equipe1.Items.Add(new ListItem(liste_equipes.ElementAt(i)[0].ToString()));
                dropdownlist_equipe2.Items.Add(new ListItem(liste_equipes.ElementAt(i)[0].ToString()));
            }
        }
    }
}