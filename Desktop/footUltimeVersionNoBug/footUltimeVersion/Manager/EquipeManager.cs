﻿using projetfoot.GestionDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace footUltimeVersion.Manager
{
    public class EquipeManager
    {
        GestionDB req = new GestionDB();

        public List<string[]> recupererListeChampionats()
        {
            List<string[]> liste_championats = req.selectRequest(@"SELECT nom_champ 
                                                                FROM CHAMPIONAT");
            
            return liste_championats;
        }

        public void ajouterEquipe(string nom, string annee, string entreneur, string stade, string capacite, string championat)
        {
            List<string[]> championatId = req.selectRequest(@"SELECT id_champ
                                                            FROM CHAMPIONAT
                                                            WHERE nom_champ = '" + championat + "'");

            string id = championatId.ElementAt(0)[0];
                                    

            req.request(@"INSERT INTO EQUIPE
                        VALUES(NULL, '" + nom +"', NULL, '" + annee + "', '" + entreneur + "', '" + stade + "', '" + capacite + "', '" + id + "' )");

            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Equipe correctement ajouté')</SCRIPT>");
        }

        public List<string[]> recupererInfoEquipe(int id)
        {
            List<string[]> infos = req.selectRequest(@"SELECT nom_equipe, annee_fond_equipe, entreneur_equipe, stade_equipe, capacite_stade_equipe, nom_champ
                                                     FROM EQUIPE E, CHAMPIONAT C
                                                     WHERE E.id_champ = C.id_champ
                                                     AND id_equip = '" + id + "'");

            return infos;

        }

        public void modifierEquipe(int id, string nom, string annee, string entreneur, string stade, string capacite, string championat)
        {
            List<string[]> championatId = req.selectRequest(@"SELECT id_champ
                                                            FROM CHAMPIONAT
                                                            WHERE nom_champ = '" + championat + "'");

            string champId = championatId.ElementAt(0)[0];

            req.request(@"UPDATE EQUIPE
                        SET nom_equipe = '" + nom + "', annee_fond_equipe = '" + annee + "', entreneur_equipe = '" + entreneur + "', stade_equipe = '" + stade + "', capacite_stade_equipe = '" + capacite + "', id_champ = '" + champId + "' WHERE id_equip = '" + id + "'");

            System.Web.HttpContext.Current.Response.Write("<SCRIPT LANGUAGE='JavaScript'>alert('Equipe a été modifiée')</SCRIPT>");
        }


    }
}