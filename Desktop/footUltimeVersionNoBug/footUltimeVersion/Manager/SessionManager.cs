﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace projetfoot.Manager
{
    static public class SessionManager
    {
        // On conserve l'id et le login de l'utilisateur en mémoire le temps de la session
        public static string id_utilisateur;
        public static string login;

        public static string Id_utilisateur
        {
            get
            {
                return id_utilisateur;
            }

            set
            {
                id_utilisateur = value;
            }
        }

        public static string Login
        {
            get
            {
                return login;
            }

            set
            {
                login = value;
            }
        }


    }
}