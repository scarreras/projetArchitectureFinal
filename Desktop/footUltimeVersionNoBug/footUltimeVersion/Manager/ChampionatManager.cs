﻿using projetfoot.GestionDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace footUltimeVersion.Manager
{
    public class ChampionatManager
    {
        GestionDB req = new GestionDB();

        public List<string[]> recupererPays()
        {
            List<string[]> liste_pays = req.selectRequest(@"SELECT nom_pays
                                                            FROM PAYS");

            return liste_pays;
        }
    }
}