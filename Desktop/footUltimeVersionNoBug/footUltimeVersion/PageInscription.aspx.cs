﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace projetfoot
{
    public partial class PageInscription : System.Web.UI.Page
    {
        Manager.UtilisateurManager utilisateur = new Manager.UtilisateurManager();
        private string pseudo;
        private string nom_util;
        private string prenom_util;
        private string mdp;
        private string mdp_confirmation;
        private string mail;
        private DateTime date_de_naissance;
        
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        //Fonction récupérant le contenu des box pour enregistrer l'utilisateur
        private void recuperer_contenu_box()
        {
            pseudo = pseudobox.Text;
            nom_util = nom_box.Text;
            prenom_util = prenom_box.Text;
            mdp = mdp_box.Text;
            mdp_confirmation =  mdpcbox.Text;
            mail = mailbox.Text;
            date_de_naissance = datenaissancebox.SelectedDate;
        }

        
        protected void Valider_Click(object sender, EventArgs e)
        {
            recuperer_contenu_box();
            utilisateur.ajouterUtilisateur(pseudo, nom_util, prenom_util, mdp, mail, date_de_naissance);
        }
        
        protected void pseudobox_TextChanged(object sender, EventArgs e)
        {
            
        }
    }
}