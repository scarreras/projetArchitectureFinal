﻿using footUltimeVersion.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace footUltimeVersion.Championnat
{
    public partial class AjouterChampionat : System.Web.UI.Page
    {
        ChampionatManager championat = new ChampionatManager();
        string nom;
        string pays;

        protected void Page_Load(object sender, EventArgs e)
        {
            afficherPays();
        }

        protected void afficherPays()
        {
            List<string[]> liste_pays = championat.recupererPays();
            dropdownlist_pays.Items.Add(new ListItem(""));

            for (int i = 0; i < liste_pays.Count; i++)
            {
                dropdownlist_pays.Items.Add(new ListItem(liste_pays.ElementAt(i)[0].ToString()));
            }
        }

        protected void button_valider(object sender, EventArgs e)
        {

        }
    }
}