﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace footUltimeVersion.Article
{
    public partial class AfficherArticle : System.Web.UI.Page
    {
        string id = "10";
        string commentaire;
        CommentaireManager manager_commentaire = new CommentaireManager();

        protected void Page_Load(object sender, EventArgs e)
        {
            this.id = Request["id"];
            genererArticle(id);
            genererCommentaires(id);
        }

        private void genererArticle(string id)
        {
            List<string[]> article = new List<string[]>();
            article = recupererUnArticle(id);

            TitreArticle.Text = article.ElementAt(0)[0];
            TextArticle.Text = article.ElementAt(0)[1];

        }

        private List<string[]> recupererUnArticle(string id)
        {
            ArticleManager manager_article = new ArticleManager();

            List<string[]> article = new List<string[]>();

            article = manager_article.recupererUnArticle(id);

            return article;
        }

        private void genererCommentaires(string id)
        {


            List<string[]> commentaires = new List<string[]>();

            commentaires = manager_commentaire.recupererListeCommentaires(id);


            afficherCommentaires(commentaires);

        }

        private void afficherCommentaires(List<string[]> commentaires)
        {

            for (int i = 0; i < commentaires.Count; i++)
            {
                HtmlGenericControl text_comment = new HtmlGenericControl("p");
                HtmlGenericControl auteur_comment = new HtmlGenericControl("p");

                System.Diagnostics.Debug.WriteLine(commentaires.ElementAt(i)[0]);
                System.Diagnostics.Debug.WriteLine(commentaires.ElementAt(i)[1]);

                text_comment.InnerHtml = commentaires.ElementAt(i)[0];
                auteur_comment.InnerHtml = commentaires.ElementAt(i)[1];
                comments.Controls.Add(auteur_comment);
                comments.Controls.Add(text_comment);
            }
        }

        protected void button_valider(object sender, EventArgs e)
        {
            commentaire = textbox_commentaire.Text;
            manager_commentaire.ajouterCommentaire(commentaire, id);
            Response.Redirect(Request.RawUrl);
        }


    }
}