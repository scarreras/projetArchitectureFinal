﻿using projetfoot.Manager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace projetfoot.Article
{
    public partial class SaisieArticle : System.Web.UI.Page
    {
        ArticleManager article = new ArticleManager();

        string titre_article;
        string texte_article;
        string description_article;

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void valider_Click(object sender, EventArgs e)
        {
            titre_article = textbox_titre.Text;
            texte_article = textarea_article.InnerText;
            description_article = textbox_description.Text;

            article.ajouterArticle(titre_article, texte_article, description_article);
            
            confirmerAjoutArticle();

        }

        private void confirmerAjoutArticle()
        {
            HtmlGenericControl messageCom = new HtmlGenericControl("p");
            messageCom.InnerHtml = "L'article a été correctement ajouté";
            corps_page.Controls.Add(messageCom);
            
        }

        protected void TextBox1_TextChanged1(object sender, EventArgs e)
        {

        }
    }
}