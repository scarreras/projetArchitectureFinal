﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Classement.aspx.cs" Inherits="footUltimeVersion.Classement" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        th {
            background-color: #239da8;
            color: white;
            font-weight: 800;
            font-size:18px;
        }
        tr:nth-child(even) {background-color: #f2f2f2}
        th, td {
    padding: 15px;
    text-align: center;
}
        table {
            width:80%;
             font-weight:700; 
             text-align:center;
             position:center;
        }
    </style>
    <div>
            <asp:GridView ID="GridView1" runat="server">
                 <Columns>
                     <asp:TemplateField>
                        <ItemTemplate><%#(int)DataBinder.Eval(Container,"RowIndex") + 1%></ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>   
        </div>
</asp:Content>
