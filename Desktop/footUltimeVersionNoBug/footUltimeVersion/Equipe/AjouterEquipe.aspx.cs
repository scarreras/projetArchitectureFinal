﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using projetfoot.Manager;
using footUltimeVersion.Manager;

namespace footUltimeVersion.Equipe
{
    public partial class AjouterEquipe : System.Web.UI.Page
    {
        EquipeManager equipe = new EquipeManager();
        string nom;
        string annee_fondation;
        string entreneur;
        string stade;
        string capacite_stade;
        string championat;

        protected void Page_Load(object sender, EventArgs e)
        {
            recupererChampionats();
            
        }

        private void recupererChampionats()
        {
            List<string[]> liste_championats = equipe.recupererListeChampionats();
            dropdownlist_championat.Items.Add(new ListItem(""));

            for (int i = 0; i < liste_championats.Count; i++)
            {
                dropdownlist_championat.Items.Add(new ListItem(liste_championats.ElementAt(i)[0].ToString()));
            }
        }

        
        protected void button_valider(object sender, EventArgs e)
        {
            nom = textbox_nom.Text;
            annee_fondation = textbox_annee.Text;
            entreneur = textbox_entreneur.Text;
            stade = textbox_stade.Text;
            capacite_stade = textbox_capacite.Text;
            championat = dropdownlist_championat.Text;

            System.Diagnostics.Debug.WriteLine(nom);
            System.Diagnostics.Debug.WriteLine(annee_fondation);
            System.Diagnostics.Debug.WriteLine(entreneur);
            System.Diagnostics.Debug.WriteLine(stade);
            System.Diagnostics.Debug.WriteLine(capacite_stade);
            System.Diagnostics.Debug.WriteLine(championat);

            equipe.ajouterEquipe(nom, annee_fondation, entreneur, stade, capacite_stade, championat);
        }

    }
}